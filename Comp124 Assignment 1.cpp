/*
Adam Ray
201271021
psaray@liverpool.ac.uk
*/


#include <stdio.h> 

#include <stdlib.h> 



int main(void)



{
	int total = 0;
	int count = 0;
	int age = 0;
	char end[] = ("\n You have paid out %d to %d people.");
	char msg[] = ("\nYou are eligible for a pension of: %d"); // declare variables in C
	char prompt[] = ("\nPlease enter your age: ");
	char layout[] = "%d";

	_asm
	{
	Initial: //This subroutine takes user input and compares it to the age boundaries to make jumps to other sub routines.
		lea eax, prompt;
		push eax;
		call printf;
		pop eax;
		lea eax, age;
		push eax;
		lea eax, layout;
		push eax;
		call scanf;
		pop eax;
		pop eax;
		cmp age, 0;
		jl exitRoutine;
		add count, 1;
		cmp age, 80;
		jge EightyPlusPension;
		cmp age, 65;
		jge SixtyFivePension;
		jl NoPension;
		jmp Initial;

	EightyPlusPension://If in the comparison age is 80 or more jumps to here
		mov eax, 120;
		push eax;
		lea eax, msg;
		push eax;
		call printf
			pop eax;
		add total, 120;
		jmp Initial;



	SixtyFivePension://If in the comparison age is 65 or more jumps to here
		mov eax, 100;
		push eax;
		lea eax, msg;
		push eax;
		call printf
			pop eax;
		add total, 100;
		jmp Initial;


	NoPension://If in the comparison age is less than 65 jumps to here
		mov eax, 0;
		push eax;
		lea eax, msg;
		push eax;
		call printf;
		pop eax;
		add total, 0;
		jmp Initial;

	exitRoutine://When a user enters a negative integer jump to here
		mov eax, count;
		push eax;
		mov eax, total;
		push eax;
		lea eax, end;
		push eax;
		call printf
			pop eax;
		pop eax;
		pop eax;



	}


	return 0;
}

